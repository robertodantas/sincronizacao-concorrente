package br.ufrn.imd.concorrente.sincronizacao.consumidor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import br.ufrn.imd.concorrente.sincronizacao.buffer.Buffer;

public class Consumidor extends Thread {
	
	private final static Logger LOGGER = Logger.getLogger(Consumidor.class.getSimpleName());
//	private final static String LOG_CONSUMIDOR = "[%s] Pedido [%s] | Elapsed Time: %3d ms | Inicio: %s | Fim: %s ";
	private final static String LOG_ELAPSED_TIME = "%3d";
	private final static int PROCESS_TIME_MIN = 30;
	private final static int PROCESS_TIME_MAX = 300;
	private Buffer buffer;
	
	public Consumidor(Integer id, Buffer buffer) {
		super("consumidor-" + id);
		this.buffer = buffer;
	}
	
	@SuppressWarnings("unused")
	@Override
	public void run() {
		
		while(true){
			
			long inicioMillis = System.currentTimeMillis();
			Date inicio = new Date();	
			
			Object obj = buffer.poll();
			
			try {
				TimeUnit.MILLISECONDS.sleep(randomMillis());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
						
			Date fim = new Date();
			long fimMillis = System.currentTimeMillis();
			
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
			
			LOGGER.info(String.format(LOG_ELAPSED_TIME, fimMillis - inicioMillis));
							
			/*LOGGER.info(String.format(LOG_CONSUMIDOR, 
					super.getName(), 
					"" + obj,
					fimMillis - inicioMillis,
					sdf.format(inicio),
					sdf.format(fim)));*/
			
		}
		
	}
	
	public int randomMillis(){
		return ThreadLocalRandom.current().nextInt(PROCESS_TIME_MIN, PROCESS_TIME_MAX + 1) + 1;
	}
	

}
