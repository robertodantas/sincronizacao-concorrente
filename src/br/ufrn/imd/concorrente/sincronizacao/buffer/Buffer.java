package br.ufrn.imd.concorrente.sincronizacao.buffer;

public interface Buffer {
	public Object poll();
	public void add(Object obj);
	public int getN();
}
