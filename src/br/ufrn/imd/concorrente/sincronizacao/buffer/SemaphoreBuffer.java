package br.ufrn.imd.concorrente.sincronizacao.buffer;

import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class SemaphoreBuffer implements Buffer {
	
	private LinkedList<Object> list;
	private Semaphore empty;
	private Semaphore mutex;
	private Semaphore full;
	private final int capactity;
		
	public SemaphoreBuffer(int capactiy) {
		this.capactity = capactiy;
		this.list = new LinkedList<>();
		this.empty = new Semaphore(capactiy);
		this.mutex = new Semaphore(1);
		this.full = new Semaphore(0);
	}
	
	@Override
	public Object poll() {
		Object obj = null;
		try{
			full.acquire();
			mutex.acquire();
				obj = list.removeFirst();
			mutex.release();
			empty.release();
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		return obj;
	}

	@Override
	public void add(Object obj) {
		try {
			empty.acquire();
			mutex.acquire();
				list.add(obj);
			mutex.release();
			full.release();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public int getN() {
		synchronized (list) {
			return list.size();
		}
	}
	
	public int getCapacity(){
		return capactity;
	}

}
