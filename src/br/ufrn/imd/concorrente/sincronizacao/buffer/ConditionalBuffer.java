package br.ufrn.imd.concorrente.sincronizacao.buffer;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionalBuffer implements Buffer {
			
	private Lock lock;
	
	private Condition cheio;
	
	private Condition vazio;
	
	private Object[] array;
	
	private Integer indice;
	
	private Integer outIndice;
	
	private Integer n;
	
	private final int capacity;
		
	public ConditionalBuffer(int capacity) {
		this.n = 0;
		this.indice = 0;
		this.outIndice = 0;
		this.array = new Object[capacity];
		this.capacity = capacity;
		this.lock = new ReentrantLock();
		this.cheio = lock.newCondition();
		this.vazio = lock.newCondition();
	}

	public void add(Object obj) {
		
		try{
			lock.lock();
			
			while(isFull()){
				cheio.awaitUninterruptibly();
			}
				
			synchronized(n){
				if(n + 1 <= capacity){
					n++;
				}
			}
						
			synchronized (indice) {
				array[indice] = obj;
				indice = ((indice + 1) % capacity);
			}
						
			if(!isEmpty()){
				vazio.signalAll();
			}
		
		}
		finally{
			lock.unlock();
		}
		
	}

	public Object poll() {
		
		Object obj = null;
		
		try{
			lock.lock();
			
			while(isEmpty()){
				vazio.awaitUninterruptibly();
			}
			
			synchronized (n) {
				if (n - 1 >= 0){
					n--;
				}
			}
			
			synchronized(outIndice){
				obj = array[outIndice];
				array[outIndice] = null;
				outIndice = ((outIndice + 1) % capacity);
			}
						
			if(!isFull()){
				cheio.signalAll();
			}
					
		}
		finally {
			lock.unlock();
		}
		
		return obj;
	}

	public synchronized int capacity() {
		return capacity;
	}	
	
	public synchronized boolean isEmpty() {
		return n == 0;
	}

	public synchronized boolean isFull() {
		return getN() == capacity();
	}
	
	public synchronized int getN(){
		return n;
	}

}
