package br.ufrn.imd.concorrente.sincronizacao.produtor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import br.ufrn.imd.concorrente.sincronizacao.buffer.Buffer;
import br.ufrn.imd.concorrente.sincronizacao.consumidor.Consumidor;

public class Produtor extends Thread {
	
	public static Integer contador = 0;

	private final static Logger LOGGER = Logger.getLogger(Consumidor.class.getName());
	private final static String LOG_PRODUTOR = "[%s] Pedido [%s] | Elapsed Time: %3d ms | Inicio: %s | Fim: %s";
	private final static int PROCESS_TIME_MIN = 30;
	private final static int PROCESS_TIME_MAX = 300;
	private Buffer buffer;
	
	public Produtor(Integer id, Buffer buffer) {
		super("produtor-" + id);
		this.buffer = buffer;
	}
	
	@Override
	public void run() {
		
		while(true){
			
			long inicioMillis = System.currentTimeMillis();
			Date inicio = new Date();
			
			Integer obj = 0;
			
			synchronized (contador) {
				obj = contador;
				contador += 1;	
			}
			
			try {
				TimeUnit.MILLISECONDS.sleep(randomMillis());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			buffer.add(obj);
												
			Date fim = new Date();
			long fimMillis = System.currentTimeMillis();
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						
			LOGGER.info(String.format(LOG_PRODUTOR, 
					super.getName(), 
					"" + obj,
					fimMillis - inicioMillis,
					sdf.format(inicio),
					sdf.format(fim)));
			
			
		}
		
	}
	
	public int randomMillis(){
		return ThreadLocalRandom.current().nextInt(PROCESS_TIME_MIN, PROCESS_TIME_MAX + 1) + 1;
	}
	
}
