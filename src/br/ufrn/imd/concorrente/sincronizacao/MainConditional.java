package br.ufrn.imd.concorrente.sincronizacao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import br.ufrn.imd.concorrente.sincronizacao.buffer.Buffer;
import br.ufrn.imd.concorrente.sincronizacao.buffer.ConditionalBuffer;
import br.ufrn.imd.concorrente.sincronizacao.consumidor.Consumidor;
import br.ufrn.imd.concorrente.sincronizacao.produtor.Produtor;

public class MainConditional {
	
	private static final String LOGGER_FORMAT = "%5$s%6$s%n";
	private static final int BUFFER_CAPACITY = 10;
	private static final int QTD_CONSUMERS = 2;
	private static final int QTD_PRODUCERS = 2;

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("java.util.logging.SimpleFormatter.format", LOGGER_FORMAT);
		
		/** Inicializa o buffer */
		Buffer buffer = new ConditionalBuffer(BUFFER_CAPACITY);
		
		/** Inicializa o buffer */
		for (int p = 0; p < BUFFER_CAPACITY; p++){
			buffer.add(p);
		}
				
		/** Inicializa os produtores */
		List<Produtor> produtores = new ArrayList<>();
		for (int p = 0; p < QTD_PRODUCERS; p++){
			produtores.add(new Produtor(p, buffer));
		}
		
		/** Inicializa os consumidores */
		List<Consumidor> consumidores = new ArrayList<>();		
		for(int j = 0; j < QTD_CONSUMERS; j++){
			consumidores.add(new Consumidor(j, buffer));
		}
		
				
		/** Inicia as threads CONSUMIDORAS */
		for(int k = 0; k < consumidores.size(); k++){
			Thread.sleep(ThreadLocalRandom.current().nextInt(20, 120));
			consumidores.get(k).start();
		}
		
//		/** Inicia as threads PRODUTORAS */
//		for(int k = 0; k < produtores.size(); k++){
//			Thread.sleep(ThreadLocalRandom.current().nextInt(20, 120));
//			produtores.get(k).start();
//		}
				
		/** Aguarda as threads */
		for(int k = 0; k < consumidores.size(); k++){
			consumidores.get(k).join();
		}	
		
//		/** Aguarda as threads */
//		for(int k = 0; k < produtores.size(); k++){
//			produtores.get(k).join();
//		}
		
	}	
	
}
